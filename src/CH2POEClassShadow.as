package {

    import flash.display.Sprite;
    import heroclickerlib.CH2;
    import models.Character;

    public class CH2POEClassShadow extends Sprite {

        public var MOD_INFO:Object =
                {
                    "name": "CH2POE - Class - Shadow",

                    "description": "Path of Exile Port for Clicker Heroes 2",
                    "version": "0.0.1",
                    "author": "Ahmad Zuhdi <ahmad.zuhdi.y@gmail.com>"
                };

        public function onStartup(game:IdleHeroMain):void { }

        public function onStaticDataLoaded(staticData:Object):void { }

        public function onUserDataLoaded():void { }

        public function onCharacterCreated(characterInstance:Character):void { }

        public function onUICreated():void { }
    }
}
